##MateRate Backend Challange \o/ - Leonardo

Para rodar o projeto, é necessário instalar o docker (e docker-compose) ou configurar uma máquina que contenha LEMP + MongoDB.

A documentação da API se encontra neste link:
https://documenter.getpostman.com/view/1954712/S1LpbCEQ

### Como rodar o projeto:

1. Primeiro clone este repositório: 

    ```git clone https://bitbucket.org/llazarini2/materate.git```

2. Após ter o código, inicialize os containers do docker (definidos no docker-compose)

    ```docker-compose up -d```

3. Instale os pacotes do PHP utilizando o composer através do container:

    ```docker-compose exec php composer install```

4. Pronto! Tudo certo, agora a API está acessível através da porta 8080.

    http://localhost:8080/api/

###Observações:

A API foi desenvolvida utilizando o laravel e possui as seguintes rotas:

URL: http://localhost:8080/api/

- Registrar Usuário: auth/register
- Logar com o Usuário: auth/login
- Retornar usuário logado: auth/user
- Retornar filmes: movies/list
- Listar Watchlist: watchlist/list
- Adicionar ou Remover filme a Watchlist: watchlist/movie

O banco de dados foi desenvolvido utilizando o MySQL + MongoDB, onde as tabelas relacionais são de usuário e filmes, já a watchlist está no BD não relacional.

###Testes:

Rodar os testes através do PHPUnit:

```docker-compose exec php phpunit```


O projeto levou cerca de 4 horas de desenvolvimento, contando com a concepção, configuração dos containers do docker, desenvolvimento da API, testes e documentação.

Agradeço a oportunidade :]
