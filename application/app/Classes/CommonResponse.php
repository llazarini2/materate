<?php
namespace App\Classes;

/**
 * Class CommonResponse
 * @package App\Classes
 */
class CommonResponse
{
    #Dados e informações que vão ser retornados por padrão
    private $message = 'Não foi possível efetuar a ação';
    private $error = '';
    private $response_status = 200;
    private $status = false;
    private $data = [];
    /**
     * Seta mensagem a ser retornada
     * @param $msg
     * @return $this
     */
    public function setMessage($msg) {
        #Seta mensagem
        $this->message = $msg;
        return $this;
    }
    /**
     * Retorna mensagem
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }
    /**
     * Seta error
     * @param $error
     * @return $this
     */
    public function setResponseStatus($st) {
        $this->response_status = $st;
        return $this;
    }
    /**
     * Seta error
     * @param $error
     * @return $this
     */
    public function setError($error) {
        $this->error = $error;
        return $this;
    }
    /**
     * Get erro
     * @return string
     */
    public function getError() {
        return $this->error;
    }
    /**
     * Retorna status
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Seta status
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        #Verifica se é um boleano
        if(!is_bool($status))
            throw new Exception("O Status deve ser um boleano.");
        #Seta status
        $this->status = $status;
        return $this;
    }
    /**
     * Seta dados
     * @param $data
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }
    /**
     * Retorna dados
     * @param $data
     * @return mixed
     */
    public function getData($data)
    {
        return $data;
    }
    /**
     * Retorna Resposta
     * @return array
     */
    public function getResponse() {
        $data = [
            'success' => $this->status,
            'message' => $this->message,
            'error' => $this->error,
            'data' => $this->data,
        ];
        #Se status for verdadeiro, não é necessária a mensagem de erro
        if($this->status === true)
        {
            unset($data['error']);

            if(!empty($this->data))
                unset($data['message']);

        }
        #Se for falso, não é necessários os dados
        else
        {
            unset($data['data']);
        }
        #Se erro estiver vazio
        if(empty($data['error']))
        {
            unset($data['error']);
        }
        return $data;
    }
}
