<?php

namespace App\Http\Controllers;

use App\Classes\CommonResponse;
use App\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{

    /**
     * List Movies with pagination
     */
    public function list() {

        //Form common response
        $commonResponse = new CommonResponse();

        //Try to get data
        try {

            //Search movies data
            $data = Movie::paginate(10);

            //Return movies
            return $commonResponse->setStatus(true)
                ->setData($data)
                ->getResponse();

        }
        catch (\Exception $e) {

            //If Error
            return $commonResponse->setStatus(false)
                ->setError($e->getMessage())
                ->getResponse();

        }

    }

}
