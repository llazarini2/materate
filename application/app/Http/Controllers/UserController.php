<?php

namespace App\Http\Controllers;

use App\Classes\CommonResponse;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{

    /**
     * Authenticate User to API
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request)
    {
        $commonResponse = new CommonResponse();

        $credentials = $request->only('email', 'password');

        try {

            //Attempt to use credentials
            if (! $token = JWTAuth::attempt($credentials)) {

                //Not valid
                return $commonResponse
                    ->setError("invalid_credentials")
                    ->setResponseStatus(400)
                    ->getResponse();

            }
        } catch (JWTException $e) {

            return $commonResponse
                ->setError("could_not_create_token")
                ->setResponseStatus(500)
                ->getResponse();
        }

        //Response Data
        return $commonResponse
            ->setStatus(true)
            ->setData([
                'token' => $token,
            ])
            ->getResponse();
    }


    /**
     * Register new User
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $commonResponse = new CommonResponse();

        //Validate
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        //Validation failed
        if($validator->fails()){

            return $commonResponse->setStatus(false)
                ->setMessage("Validation errors.")
                ->setError($validator->errors())
                ->getResponse();

        }

        //Create a new User
        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        //Get Token
        $token = JWTAuth::fromUser($user);

        //Response Data
        return $commonResponse
            ->setStatus(true)
            ->setData([
                'user' => $user,
                'token' => $token,
            ])
            ->getResponse();
    }

    /**
     * Get User Authenticate
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthenticatedUser()
    {

        $commonResponse = new CommonResponse();

        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {

                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return $commonResponse->setStatus(true)
            ->setData($user)
            ->getResponse();
    }
}
