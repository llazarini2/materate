<?php

namespace App\Http\Controllers;

use App\Classes\CommonResponse;
use App\Movie;
use App\UserMovie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class WatchListController extends Controller
{

    /**
     * List Movies with pagination
     */
    public function list() {

        //Form common response
        $commonResponse = new CommonResponse();

        //Try to get data
        try {

            //Get user and movie
            $user = JWTAuth::parseToken()->authenticate();

            //Search watchlist data
            $data = UserMovie::with('movie')
                ->where("user_id", $user->id)
                ->paginate(10);

            //Return movies
            return $commonResponse->setStatus(true)
                ->setData($data)
                ->getResponse();

        }
        catch (\Exception $e) {

            //If Error
            return $commonResponse->setStatus(false)
                ->setError($e->getMessage())
                ->getResponse();

        }

    }


    /**
     * Remove a movie from watchlist
     */
    public function addOrRemove(Request $request) {

        //Form common response
        $commonResponse = new CommonResponse();

        //Validate
        $validator = Validator::make($request->all(), [
            'movie_id' => 'required|int',
            'watchlist' => 'required|int'
        ]);

        //Try to get data
        try {

            //Validation failed
            if($validator->fails())
                return $commonResponse->setStatus(false)
                    ->setMessage("Validation errors.")
                    ->setError($validator->errors())
                    ->getResponse();


            //Get user and movie
            $user = JWTAuth::parseToken()->authenticate();
            $movieId = $request->get('movie_id');
            $add = $request->get('watchlist');

            //Verify if movie exists
            if(!Movie::find($movieId))
                return $commonResponse->setStatus(false)
                    ->setMessage("This movie doesn't exists.")
                    ->getResponse();


            //If to remove movie
            if(!$add)
            {
                //Removie movie from watchlist
                $data = UserMovie::where('user_id', $user->id)
                    ->where('movie_id', $movieId)
                    ->delete();

                //Show response
                return $commonResponse->setStatus(true)
                    ->setMessage("Movie removed from your watchlist.")
                    ->getResponse();
            }

            //Add movie to watchlist
            UserMovie::firstOrCreate([
                'user_id' => $user->id,
                'movie_id' => $movieId,
            ]);

            //Show response
            return $commonResponse->setStatus(true)
                ->setMessage("Movie added to your watchlist.")
                ->getResponse();

        }
        catch (\Exception $e) {

            //If Error
            return $commonResponse->setStatus(false)
                ->setError($e->getMessage())
                ->getResponse();

        }

    }

}
