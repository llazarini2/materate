<?php

namespace App\Http\Middleware;

use App\Classes\CommonResponse;
use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware extends BaseMiddleware
{

    /**
     * Handle tokens JWT
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $commonResponse = new CommonResponse();


        try {

            //Try to get authentication
            $user = JWTAuth::parseToken()->authenticate();

        }
        catch (Exception $e)
        {


            //Is invalid
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {

                return response()->json(
                    $commonResponse->setStatus(false)
                        ->setError(['status' => 'Token is Invalid'])
                        ->getResponse()
                );

            //Token Expired
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {

                return response()->json(
                    $commonResponse->setStatus(false)
                        ->setError(['status' => 'Token is Expired'])
                        ->getResponse()
                );

            //Authorization not found
            } else {

                return response()->json(
                    $commonResponse->setStatus(false)
                        ->setError(['status' => 'Authorization Token not found'])
                        ->getResponse()
                );
            }
        }

        //Next request
        return $next($request);
    }
}
