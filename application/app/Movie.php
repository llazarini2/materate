<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\HybridRelations;

class Movie extends Model
{
    use HybridRelations;

    protected $connection = "mysql";

}
