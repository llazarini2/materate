<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class UserMovie extends Eloquent
{
    use HybridRelations;

    protected $connection = "mongodb";
    protected $collection = 'user_movies';

    protected $fillable = ['user_id', 'movie_id'];

    public function movie(){
        return $this->belongsTo('App\Movie');
    }

}
