<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'middleware' => 'api',
    'prefix' => ''
], function ($router) {

    //Auth register and login
    Route::post('auth/register', 'UserController@register');
    Route::post('auth/login', 'UserController@authenticate');

    //Auth group
    Route::group(['middleware' => ['jwt.verify']], function() {

        //Get current user
        Route::get('auth/user', 'UserController@getAuthenticatedUser');

        //Movies List
        Route::get('movies/list', 'MovieController@list');

        //Watchlist
        Route::get('watchlist/list', 'WatchListController@list');
        Route::post('watchlist/movie', 'WatchListController@addOrRemove');


    });

});


