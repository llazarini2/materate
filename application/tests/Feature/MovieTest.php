<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MovieTest extends TestCase
{
    //Token
    private $token = "";

    /**
     * Get Login Token to test
     * @return string
     */
    private function getToken(){

        $response = $this->post('/api/auth/login', ["email" => "CHALLENGE@materate.com", "password" => "mateRATEbackEND"]);

        $jsonResponse = json_decode($response->getContent());
        $this->token = $jsonResponse->data->token;

        return "?token={$this->token}";

    }

    /**
     * Test Movies List
     * @return void
     */
    public function testListMovies()
    {
        $response = $this->get('/api/movies/list' . $this->getToken());

        $response->assertStatus(200);

        $response->assertJson([
            "success" => true,
        ]);
    }
}
