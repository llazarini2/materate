<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WatchListTest extends TestCase
{
    //Token
    private $token = "";

    /**
     * Get Login Token to test
     * @return string
     */
    private function getToken(){

        $response = $this->post('/api/auth/login', ["email" => "CHALLENGE@materate.com", "password" => "mateRATEbackEND"]);

        $jsonResponse = json_decode($response->getContent());
        $this->token = $jsonResponse->data->token;

        return "?token={$this->token}";

    }


    /**
     * Test WatchList List
     * @return void
     */
    public function testListWatchList()
    {
        $response = $this->get('/api/watchlist/list' . $this->getToken());

        $response->assertStatus(200);
    }


    /**
     * Test WatchList List
     * @return void
     */
    public function testAddToWatchList()
    {
        $response = $this->post('/api/watchlist/movie' . $this->getToken(), ['movie_id' => 1, 'watchlist' => 1]);

        $response->assertStatus(200);

        $response->assertJson([
            "success" => true,
        ]);
    }

    /**
     * Test WatchList Remove
     * @return void
     */
    public function testRemoveToWatchList()
    {
        $response = $this->post('/api/watchlist/movie' . $this->getToken(), ['watchlist' => 0, 'movie_id' => 1]);

        $response->assertStatus(200);

        $response->assertJson([
            "success" => true,
        ]);
    }
}
